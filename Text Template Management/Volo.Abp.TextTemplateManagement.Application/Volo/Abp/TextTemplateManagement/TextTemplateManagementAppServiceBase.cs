﻿using Volo.Abp.Application.Services;
using Volo.Abp.TextTemplateManagement.Localization;

namespace Volo.Abp.TextTemplateManagement
{
    public abstract class TextTemplateManagementAppServiceBase : ApplicationService
	{
		protected TextTemplateManagementAppServiceBase()
		{
			base.LocalizationResource = typeof(TextTemplateManagementResource);
			base.ObjectMapperContext = typeof(TextTemplateManagementApplicationModule);
		}
	}
}

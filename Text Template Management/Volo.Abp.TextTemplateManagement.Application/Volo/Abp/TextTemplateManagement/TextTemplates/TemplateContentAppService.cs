﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.TextTemplateManagement.Authorization;
using Volo.Abp.TextTemplating;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    [Authorize(TextTemplateManagementPermissions.TextTemplates.Default)]
	public class TemplateContentAppService : TextTemplateManagementAppServiceBase, IRemoteService, IApplicationService, ITemplateContentAppService
	{

		private readonly ITextTemplateContentRepository _textTemplateContentRepository;

		private readonly ITemplateContentProvider _templateContentProvider;

		private readonly ITemplateDefinitionManager _templateDefinitionManager;

		public TemplateContentAppService(ITextTemplateContentRepository textTemplateContentRepository, ITemplateContentProvider templateContentProvider, ITemplateDefinitionManager templateDefinitionManager)
		{
			this._textTemplateContentRepository = textTemplateContentRepository;
			this._templateContentProvider = templateContentProvider;
			this._templateDefinitionManager = templateDefinitionManager;
		}

		public virtual async Task<TextTemplateContentDto> GetAsync(GetTemplateContentInput input)
		{
			var templateDefinition = this._templateDefinitionManager.Get(input.TemplateName);
			string content = await this._templateContentProvider.GetContentOrNullAsync(templateDefinition, input.CultureName, true, false);
			return new TextTemplateContentDto
			{
				CultureName = input.CultureName,
				Content = content,
				Name = input.TemplateName
			};
		}

		[Authorize(TextTemplateManagementPermissions.TextTemplates.EditContents)]
		public virtual async Task RestoreToDefaultAsync(RestoreTemplateContentInput input)
		{
			var templateDefinition = this._templateDefinitionManager.Get(input.TemplateName);
			var textTemplateContent = await this._textTemplateContentRepository.FindAsync(templateDefinition.Name, input.CultureName);
			if (textTemplateContent != null)
			{
				await this._textTemplateContentRepository.DeleteAsync(textTemplateContent);
			}
		}

		[Authorize(TextTemplateManagementPermissions.TextTemplates.EditContents)]
		public virtual async Task<TextTemplateContentDto> UpdateAsync(UpdateTemplateContentInput input)
		{
			var templateDefinition = this._templateDefinitionManager.Get(input.TemplateName);
			var textTemplateContent = await this._textTemplateContentRepository.FindAsync(input.TemplateName, input.CultureName);
			if (textTemplateContent != null)
			{
				await this._textTemplateContentRepository.DeleteAsync(textTemplateContent, true);
			}
			string text = await this._templateContentProvider.GetContentOrNullAsync(templateDefinition, input.CultureName, true, false);
			TextTemplateContentDto result;
			if (text == input.Content)
			{
				result = new TextTemplateContentDto
				{
					Content = text,
					Name = input.TemplateName,
					CultureName = input.CultureName
				};
			}
			else
			{
				var newTextTemplateContent = new TextTemplateContent(Guid.NewGuid(), input.TemplateName, input.Content, input.CultureName, base.CurrentTenant.Id);
				await this._textTemplateContentRepository.InsertAsync(newTextTemplateContent);
				result = new TextTemplateContentDto
				{
					Content = input.Content,
					Name = input.TemplateName,
					CultureName = input.CultureName
				};
			}
			return result;
		}
	}
}

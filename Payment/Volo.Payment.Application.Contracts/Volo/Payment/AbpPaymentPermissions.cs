﻿namespace Volo.Payment
{
    public class AbpPaymentPermissions
	{
		public static string[] GetAll()
		{
			return new string[]
			{
				"AbpPayment"
			};
		}

		public const string GroupName = "AbpPayment";
	}
}

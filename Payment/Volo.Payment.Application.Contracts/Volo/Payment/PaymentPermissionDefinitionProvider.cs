﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Payment.Localization;

namespace Volo.Payment
{
    public class PaymentPermissionDefinitionProvider : PermissionDefinitionProvider
	{
		public override void Define(IPermissionDefinitionContext context)
		{
		}

		private static LocalizableString L(string name)
		{
			return LocalizableString.Create<PaymentResource>(name);
		}
	}
}

﻿using System;
using System.Linq;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Payment.Requests;

namespace Volo.Payment.EntityFrameworkCore
{
    public class EfCorePaymentRequestRepository : EfCoreRepository<IPaymentDbContext, PaymentRequest, Guid>, IReadOnlyBasicRepository<PaymentRequest, Guid>, IReadOnlyBasicRepository<PaymentRequest>, IBasicRepository<PaymentRequest>, IBasicRepository<PaymentRequest, Guid>, IRepository, IPaymentRequestRepository
	{
		public EfCorePaymentRequestRepository(IDbContextProvider<IPaymentDbContext> dbContextProvider)
			: base(dbContextProvider)
		{
		}

		public override IQueryable<PaymentRequest> WithDetails()
		{
			return this.GetQueryable().IncludeDetails();
		}
	}
}

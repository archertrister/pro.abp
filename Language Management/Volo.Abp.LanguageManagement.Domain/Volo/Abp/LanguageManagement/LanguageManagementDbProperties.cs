﻿using Volo.Abp.Data;

namespace Volo.Abp.LanguageManagement
{
    public static class LanguageManagementDbProperties
	{
		public const string ConnectionStringName = "AbpLanguageManagement";

		public static string DbTablePrefix { get; set; }

		public static string DbSchema { get; set; }

		static LanguageManagementDbProperties()
		{
			DbTablePrefix = AbpCommonDbProperties.DbTablePrefix;
			DbSchema = AbpCommonDbProperties.DbSchema;
		}
	}
}

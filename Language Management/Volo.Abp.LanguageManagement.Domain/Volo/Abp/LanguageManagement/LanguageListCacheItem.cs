﻿using System;
using System.Collections.Generic;
using Volo.Abp.Caching;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    [CacheName("Volo.Abp.LanguageList")]
	[Serializable]
	public class LanguageListCacheItem
	{
		public List<LanguageInfo> Languages { get; set; }

		public LanguageListCacheItem(List<LanguageInfo> languages)
		{
			this.Languages = languages;
		}
	}
}

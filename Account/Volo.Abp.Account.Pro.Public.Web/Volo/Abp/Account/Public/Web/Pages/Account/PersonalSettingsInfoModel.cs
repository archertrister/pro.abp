﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Identity;
using Volo.Abp.Validation;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class PersonalSettingsInfoModel
	{
		[Required]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxUserNameLength", null)]
		[Display(Name = "DisplayName:UserName")]
		public string UserName { get; set; }

		[Required]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxEmailLength", null)]
		[Display(Name = "DisplayName:Email")]
		public string Email { get; set; }

		[Display(Name = "DisplayName:Name")]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxNameLength", null)]
		public string Name { get; set; }

		[Display(Name = "DisplayName:Surname")]
		[DynamicStringLength(typeof(IdentityUserConsts), "MaxSurnameLength", null)]
		public string Surname { get; set; }

		[DynamicStringLength(typeof(IdentityUserConsts), "MaxPhoneNumberLength", null)]
		[Display(Name = "DisplayName:PhoneNumber")]
		public string PhoneNumber { get; set; }

		public bool PhoneNumberConfirmed { get; set; }
	}
}

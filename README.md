# pro.abp

#### 介绍
缘由：由于abp vnext商业源码版价格太贵，需要5k刀，还不卖给我，让人火大。可很多资源都可以直接从官网获取，比如商业版的dll可以从https://abp.io/packages获取，模块说明可以从https://docs.abp.io/api-docs/commercial/3.0/api/index.html获取，那我就直接拉下来，反编译后学习他的思路，撸出自己的源码出来，放到开源社区，供广大.net爱好者学习，也欢迎大家参与修复存在的bug，新增新功能模块。

声明：本仓库的开源代码，只为了供大家研究、学习，不允许商业使用，如果由于用于商业项目带来了法律风险，本仓库概不负责，由相应的法律主体承担所有责任。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

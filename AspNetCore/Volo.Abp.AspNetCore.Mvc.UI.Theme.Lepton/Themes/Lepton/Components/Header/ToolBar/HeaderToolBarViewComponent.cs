﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Header.ToolBar
{
	public class HeaderToolBarViewComponent : LeptonViewComponentBase
	{
		private readonly IToolbarManager _toolbarManager;

		public HeaderToolBarViewComponent(IToolbarManager toolbarManager)
		{
			this._toolbarManager = toolbarManager;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var model = await this._toolbarManager.GetAsync("Main");
			return base.View<Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars.Toolbar>("~/Themes/Lepton/Components/Header/ToolBar/Default.cshtml", model);
		}
	}
}
